package main

import (
	app2 "cwtch.im/cwtch/app"
	"cwtch.im/cwtch/app/utils"
	"cwtch.im/cwtch/event"
	"git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	"path"
)

func main() {

	// System Setup, We need Tor and Logging up and Running
	log.AddEverythingFromPattern("peer/alice")
	log.SetLevel(log.LevelDebug)

	acn, err := tor.NewTorACN(path.Join(".", ".cwtch"), "")
	if err != nil {
		log.Errorf("\nError connecting to Tor: %v\n", err)
		os.Exit(1)
	}

	app := app2.NewApp(acn, ".")
	app.CreatePeer("alice", "be gay, do crimes")
	alice := utils.WaitGetPeer(app, "alice")
	app.LaunchPeers()
	eventBus := app.GetEventBus(alice.GetOnion())
	queue := event.NewQueue()
	eventBus.Subscribe(event.NewMessageFromPeer, queue)

	// For every new Data Packet Alice received she will Print it out.
	for {
		event := queue.Next()
		log.Printf(log.LevelInfo, "Received %v from %v: %s", event.EventType, event.Data["Onion"], event.Data["Data"])
	}
}

package app

import (
	"cwtch.im/cwtch/event"
	"git.openprivacy.ca/openprivacy/connectivity"
	"git.openprivacy.ca/openprivacy/log"
	"sync"

	"cwtch.im/cwtch/app/plugins"
	"cwtch.im/cwtch/peer"
)

type appletPeers struct {
	peerLock sync.Mutex
	peers    map[string]peer.CwtchPeer
	launched bool // bit hacky, place holder while we transition to full multi peer support and a better api
}

type appletACN struct {
	acn connectivity.ACN
}

type appletPlugins struct {
	plugins sync.Map //map[string] []plugins.Plugin
}

// ***** applet ACN

func (a *appletACN) init(acn connectivity.ACN, publish func(int, string)) {
	a.acn = acn
	acn.SetStatusCallback(publish)
	prog, status := acn.GetBootstrapStatus()
	publish(prog, status)
}

func (a *appletACN) Shutdown() {
	a.acn.Close()
}

// ***** appletPeers

func (ap *appletPeers) init() {
	ap.peers = make(map[string]peer.CwtchPeer)
	ap.launched = false
}

// LaunchPeers starts each peer Listening and connecting to peers and groups
func (ap *appletPeers) LaunchPeers() {
	log.Debugf("appletPeers LaunchPeers\n")
	ap.peerLock.Lock()
	defer ap.peerLock.Unlock()
	if ap.launched {
		return
	}
	for pid, p := range ap.peers {
		log.Debugf("Launching %v\n", pid)
		p.Listen()
		log.Debugf("done Listen() for %v\n", pid)
		p.StartPeersConnections()
		log.Debugf("done StartPeersConnections() for %v\n", pid)
	}
	ap.launched = true
}

// ListPeers returns a map of onions to their profile's Name
func (ap *appletPeers) ListPeers() map[string]string {
	keys := map[string]string{}

	ap.peerLock.Lock()
	defer ap.peerLock.Unlock()
	for k, p := range ap.peers {
		keys[k] = p.GetName()
	}
	return keys
}

// GetPeer returns a cwtchPeer for a given onion address
func (ap *appletPeers) GetPeer(onion string) peer.CwtchPeer {
	if peer, ok := ap.peers[onion]; ok {
		return peer
	}
	return nil
}

// ***** applet Plugins

func (ap *appletPlugins) Shutdown() {
	log.Debugf("shutting down applet plugins...")
	ap.plugins.Range(func(k, v interface{}) bool {
		log.Debugf("shutting down plugins for %v", k)
		ap.ShutdownPeer(k.(string))
		return true
	})
}

func (ap *appletPlugins) ShutdownPeer(peerid string) {
	log.Debugf("shutting down plugins for %v", peerid)
	pluginsI, ok := ap.plugins.Load(peerid)
	if ok {
		plugins := pluginsI.([]plugins.Plugin)
		for _, plugin := range plugins {
			log.Debugf("shutting down plugin: %v", plugin)
			plugin.Shutdown()
		}
	}
}

func (ap *appletPlugins) AddPlugin(peerid string, id plugins.PluginID, bus event.Manager, acn connectivity.ACN) {
	if _, exists := ap.plugins.Load(peerid); !exists {
		ap.plugins.Store(peerid, []plugins.Plugin{})
	}

	pluginsinf, _ := ap.plugins.Load(peerid)
	peerPlugins := pluginsinf.([]plugins.Plugin)

	newp := plugins.Get(id, bus, acn)
	newp.Start()
	peerPlugins = append(peerPlugins, newp)
	log.Debugf("storing plugin for %v %v", peerid, peerPlugins)
	ap.plugins.Store(peerid, peerPlugins)
}

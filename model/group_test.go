package model

import (
	"cwtch.im/cwtch/protocol/groups"
	"testing"
	"time"
)

func TestGroup(t *testing.T) {
	g, _ := NewGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	dgm := &groups.DecryptedGroupMessage{
		Onion:              "onion",
		Text:               "Hello World!",
		Timestamp:          uint64(time.Now().Unix()),
		SignedGroupID:      []byte{},
		PreviousMessageSig: []byte{},
		Padding:            []byte{},
	}
	encMessage, _ := g.EncryptMessage(dgm)
	ok, message := g.DecryptMessage(encMessage)
	if !ok || message.Text != "Hello World!" {
		t.Errorf("group encryption was invalid, or returned wrong message decrypted:%v message:%v", ok, message)
		return
	}
	g.SetAttribute("test", "test_value")
	value, exists := g.GetAttribute("test")
	if !exists || value != "test_value" {
		t.Errorf("Custom Attribute Should have been set, instead %v %v", exists, value)
	}
	t.Logf("Got message %v", message)
}

func TestGroupErr(t *testing.T) {
	_, err := NewGroup("not a real group name")
	if err == nil {
		t.Errorf("Group Setup Should Have Failed")
	}
}

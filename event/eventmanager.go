package event

import (
	"crypto/rand"
	"math"
	"math/big"
	"sync"
)

// Event is a structure which binds a given set of data to an Type
type Event struct {
	EventType Type
	EventID   string
	Data      map[Field]string
}

// GetRandNumber is a helper function which returns a random integer, this is
// currently mostly used to generate messageids
func GetRandNumber() *big.Int {
	num, err := rand.Int(rand.Reader, big.NewInt(math.MaxUint32))
	// If we can't generate random numbers then panicking is probably
	// the best option.
	if err != nil {
		panic(err.Error())
	}
	return num
}

// NewEvent creates a new event object with a unique ID and the given type and data.
func NewEvent(eventType Type, data map[Field]string) Event {
	return Event{EventType: eventType, EventID: GetRandNumber().String(), Data: data}
}

// NewEventList creates a new event object with a unique ID and the given type and data supplied in a list format and composed into a map of Type:string
func NewEventList(eventType Type, args ...interface{}) Event {
	data := map[Field]string{}
	for i := 0; i < len(args); i += 2 {
		key, kok := args[i].(Field)
		val, vok := args[i+1].(string)
		if kok && vok {
			data[key] = val
		}
	}
	return Event{EventType: eventType, EventID: GetRandNumber().String(), Data: data}
}

// Manager is an Event Bus which allows subsystems to subscribe to certain EventTypes and publish others.
type manager struct {
	subscribers map[Type][]Queue
	events      chan Event
	mapMutex    sync.Mutex
	internal    chan bool
	closed      bool
}

// Manager is an interface for an event bus
// FIXME this interface lends itself to race conditions around channels
type Manager interface {
	Subscribe(Type, Queue)
	Publish(Event)
	PublishLocal(Event)
	Shutdown()
}

// NewEventManager returns an initialized EventManager
func NewEventManager() Manager {
	em := &manager{}
	em.initialize()
	return em
}

// Initialize sets up the Manager.
func (em *manager) initialize() {
	em.subscribers = make(map[Type][]Queue)
	em.events = make(chan Event)
	em.internal = make(chan bool)
	em.closed = false
	go em.eventBus()
}

// Subscribe takes an eventType and an Channel and associates them in the eventBus. All future events of that type
// will be sent to the eventChannel.
func (em *manager) Subscribe(eventType Type, queue Queue) {
	em.mapMutex.Lock()
	defer em.mapMutex.Unlock()
	em.subscribers[eventType] = append(em.subscribers[eventType], queue)
}

// Publish takes an Event and sends it to the internal eventBus where it is distributed to all Subscribers
func (em *manager) Publish(event Event) {
	if event.EventType != "" && em.closed != true {
		em.events <- event
	}
}

// Publish an event only locally, not going over an IPC bridge if there is one
func (em *manager) PublishLocal(event Event) {
	em.Publish(event)
}

// eventBus is an internal function that is used to distribute events to all subscribers
func (em *manager) eventBus() {
	for {
		event := <-em.events

		// In the case on an empty event. Teardown the Queue
		if event.EventType == "" {
			break
		}

		// maps aren't thread safe
		em.mapMutex.Lock()
		subscribers := em.subscribers[event.EventType]
		em.mapMutex.Unlock()

		// Send the event to any subscribers to that event type
		for _, subscriber := range subscribers {
			subscriber.Publish(event)
		}
	}

	// We are about to exit the eventbus thread, fire off an event internally
	em.internal <- true
}

// Shutdown triggers, and waits for, the internal eventBus goroutine to finish
func (em *manager) Shutdown() {
	em.events <- Event{}
	em.closed = true
	// wait for eventBus to finish
	<-em.internal
	close(em.events)
	close(em.internal)
}

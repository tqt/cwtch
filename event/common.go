package event

// Type captures the definition of many common Cwtch application events
type Type string

// Defining Common Event Types
const (
	StatusRequest        = Type("StatusRequest")
	ProtocolEngineStatus = Type("ProtocolEngineStatus")

	// Attempt to outbound peer with a given remote peer
	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"
	PeerRequest = Type("PeerRequest")

	// RetryPeerRequest
	// Identical to PeerRequest, but allows Engine to make decisions regarding blocked peers
	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"
	RetryPeerRequest = Type("RetryPeerRequest")

	// RemotePeer
	// Authorization(model.peer.Auth_...)
	SetPeerAuthorization = Type("UpdatePeerAuthorization")

	// Turn on/off blocking of unknown peers (if peers aren't in the contact list then they will be autoblocked
	BlockUnknownPeers = Type("BlockUnknownPeers")
	AllowUnknownPeers = Type("AllowUnknownPeers")

	// GroupServer
	JoinServer = Type("JoinServer")

	// attributes GroupServer - the onion of the server to leave
	LeaveServer = Type("LeaveServer")

	ProtocolEngineStartListen = Type("ProtocolEngineStartListen")
	ProtocolEngineStopped     = Type("ProtocolEngineStopped")

	InvitePeerToGroup = Type("InvitePeerToGroup")

	// a group invite has been received from a remote peer
	// attributes:
	// TimestampReceived [eg time.Now().Format(time.RFC3339Nano)]
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"]
	// GroupInvite: [eg "torv3....."]
	// Imported
	NewGroupInvite = Type("NewGroupInvite")

	// GroupID
	AcceptGroupInvite = Type("AcceptGroupInvite")

	SendMessageToGroup = Type("SendMessagetoGroup")

	//Ciphertext, Signature:
	EncryptedGroupMessage = Type("EncryptedGroupMessage")
	//TimestampReceived, TimestampSent, Data(Message), GroupID, Signature, PreviousSignature, RemotePeer
	NewMessageFromGroup = Type("NewMessageFromGroup")

	// an error was encountered trying to send a particular Message to a group
	// attributes:
	// GroupServer: The server the Message was sent to
	// Signature: The signature of the Message that failed to send
	// Error: string describing the error
	SendMessageToGroupError = Type("SendMessageToGroupError")

	SendMessageToPeer  = Type("SendMessageToPeer")
	NewMessageFromPeer = Type("NewMessageFromPeer")

	// RemotePeer, scope, path
	NewGetValMessageFromPeer = Type("NewGetValMessageFromPeer")

	// RemotePeer, val, exists
	SendRetValMessageToPeer = Type("SendRetValMessageToPeer")

	// RemotePeer, scope, val
	SendGetValMessageToPeer = Type("SendGetValMessageToPeer")

	// RemotePeer, scope, path, data, exists
	NewRetValMessageFromPeer = Type("NewRetValMessageFromPeer")

	// Peer acknowledges a previously sent message
	// attributes
	// EventID: The original event id that the peer is responding too.
	// RemotePeer: The peer associated with the acknowledgement
	PeerAcknowledgement = Type("PeerAcknowledgement")

	// attributes:
	// RemotePeer: [eg "chpr7qm6op5vfcg2pi4vllco3h6aa7exexc4rqwnlupqhoogx2zgd6qd"]
	// Error: string describing the error
	SendMessageToPeerError = Type("SendMessageToPeerError")

	// REQUESTS TO STORAGE ENGINE

	// a peer contact has been added
	// attributes:
	// RemotePeer [eg ""]
	// Authorization
	PeerCreated = Type("PeerCreated")

	// Password, NewPassword
	ChangePassword = Type("ChangePassword")

	// Error(err), EventID
	ChangePasswordError = Type("ChangePasswordError")

	// EventID
	ChangePasswordSuccess = Type("ChangePasswordSuccess")

	// a group has been successfully added or newly created
	// attributes:
	// Data [serialized *model.Group]
	GroupCreated = Type("GroupCreated")

	// RemotePeer
	DeleteContact = Type("DeleteContact")

	// GroupID
	DeleteGroup = Type("DeleteGroup")

	// change the .Name attribute of a profile (careful - this is not a custom attribute. it is used in the underlying protocol during handshakes!)
	// attributes:
	// ProfileName [eg "erinn"]
	SetProfileName = Type("SetProfileName")

	// request to store a profile-wide attribute (good for e.g. per-profile settings like theme prefs)
	// attributes:
	// Key [eg "fontcolor"]
	// Data [eg "red"]
	SetAttribute = Type("SetAttribute")

	// request to store a per-contact attribute (e.g. display names for a peer)
	// attributes:
	// RemotePeer [eg ""]
	// Key [eg "nick"]
	// Data [eg "erinn"]
	SetPeerAttribute = Type("SetPeerAttribute")

	// request to store a per-cwtch-group attribute (e.g. display name for a group)
	// attributes:
	// GroupID [eg ""]
	// Key [eg "nick"]
	// Data [eg "open privacy board"]
	SetGroupAttribute = Type("SetGroupAttribute")

	// PeerStateChange servers as a new incoming connection message as well, and can/is consumed by frontends to alert of new p2p connections
	// RemotePeer
	// ConnectionState
	PeerStateChange = Type("PeerStateChange")

	// GroupServer
	// ConnectionState
	ServerStateChange = Type("ServerStateChange")

	/***** Application client / service messages *****/

	// ProfileName, Password, Data(tag)
	CreatePeer = Type("CreatePeer")

	// service -> client: Identity(localId), Password, [Status(new/default=blank || from reload='running')]
	// app -> Key, Salt
	NewPeer = Type("NewPeer")

	// Identity(onion)
	DeletePeer = Type("DeletePeer")

	// Identity(onion), Data(pluginID)
	AddPeerPlugin = Type("AddPeerPlugin")

	// Password
	LoadProfiles = Type("LoadProfiles")

	// Client has reloaded, triggers NewPeer s then ReloadDone
	ReloadClient = Type("ReloadClient")

	ReloadDone = Type("ReloadDone")

	// Identity - Ask service to resend all connection states
	ReloadPeer = Type("ReloadPeer")

	// Identity(onion)
	ShutdownPeer = Type("ShutdownPeer")

	Shutdown = Type("Shutdown")

	// Error(err)
	// Error creating peer
	PeerError = Type("PeerError")

	// Error(err)
	AppError = Type("AppError")

	GetACNStatus = Type("GetACNStatus")

	// Progress, Status
	ACNStatus = Type("ACNStatus")

	// Network Status
	// Status: Success || Error
	// Error: Description of the Error
	// Onion: the local onion we attempt to check
	NetworkStatus = Type("NetworkError")

	// For debugging. Allows test to emit a Syn and get a response Ack(eventID) when the subsystem is done processing a queue
	Syn = Type("Syn")
	Ack = Type("Ack")
)

// Field defines common event attributes
type Field string

// Defining Common Field Types
const (

	// A peers local onion address
	Onion = Field("Onion")

	RemotePeer        = Field("RemotePeer")
	Ciphertext        = Field("Ciphertext")
	Signature         = Field("Signature")
	PreviousSignature = Field("PreviousSignature")
	TimestampSent     = Field("TimestampSent")
	TimestampReceived = Field("TimestampReceived")

	Identity = Field("Identity")

	GroupID          = Field("GroupID")
	GroupServer      = Field("GroupServer")
	ServerTokenY     = Field("ServerTokenY")
	ServerTokenOnion = Field("ServerTokenOnion")
	GroupInvite      = Field("GroupInvite")

	ProfileName = Field("ProfileName")
	Password    = Field("Password")
	NewPassword = Field("NewPassword")

	ConnectionState = Field("ConnectionState")

	Key    = Field("Key")
	Data   = Field("Data")
	Scope  = Field("Scope")
	Path   = Field("Path")
	Exists = Field("Exists")

	Salt = Field("Salt")

	Error = Field("Error")

	Progreess    = Field("Progress")
	Status       = Field("Status")
	EventID      = Field("EventID")
	EventContext = Field("EventContext")

	Authorization = Field("Authorization")

	KeyBundle = Field("KeyBundle")

	// Indicate whether an event was triggered by a user import
	Imported = Field("Imported")
)

// Defining Common errors
const (
	AppErrLoaded0 = "Loaded 0 profiles"
)

// Values to be suplied in event.NewPeer for Status
const (
	StorageRunning = "running"
	StorageNew     = "new"
)

// Defining Protocol Contexts
const (
	ContextAck    = "im.cwtch.acknowledgement"
	ContextInvite = "im.cwtch.invite"
	ContextRaw    = "im.cwtch.raw"
	ContextGetVal = "im.cwtch.getVal"
	ContextRetVal = "im.cwtch.retVal"
)

// Define Default Attribute Keys
const (
	SaveHistoryKey = "SavePeerHistory"
)

// Define Default Attribute Values
const (
	// Save History has 3 distinct states. By default we don't save history (DefaultDeleteHistory), if the peer confirms this
	// we change to DeleteHistoryConfirmed, if they confirm they want to save then this becomes SaveHistoryConfirmed
	// We use this distinction between default and confirmed to drive UI
	DeleteHistoryDefault   = "DefaultDeleteHistory"
	SaveHistoryConfirmed   = "SaveHistory"
	DeleteHistoryConfirmed = "DeleteHistoryConfirmed"
)

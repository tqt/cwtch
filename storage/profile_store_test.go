// Known race issue with event bus channel closure

package storage

import (
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/storage/v0"
	"fmt"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	"testing"
	"time"
)

const testingDir = "./testing"
const filenameBase = "testStream"
const password = "asdfqwer"
const line1 = "Hello from storage!"
const testProfileName = "Alice"
const testKey = "key"
const testVal = "value"
const testInitialMessage = "howdy"
const testMessage = "Hello from storage"

func TestProfileStoreUpgradeV0toV1(t *testing.T) {
	log.SetLevel(log.LevelDebug)
	os.RemoveAll(testingDir)
	eventBus := event.NewEventManager()

	queue := event.NewQueue()
	eventBus.Subscribe(event.ChangePasswordSuccess, queue)

	fmt.Println("Creating and initializing v0 profile and store...")
	profile := NewProfile(testProfileName)
	ps1 := v0.NewProfileWriterStore(eventBus, testingDir, password, profile)

	groupid, invite, err := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	if err != nil {
		t.Errorf("Creating group: %v\n", err)
	}
	if err != nil {
		t.Errorf("Creating group invite: %v\n", err)
	}

	ps1.AddGroup(invite, profile.Onion)

	fmt.Println("Sending 200 messages...")

	for i := 0; i < 200; i++ {
		ps1.AddGroupMessage(groupid, time.Now().Format(time.RFC3339Nano), time.Now().Format(time.RFC3339Nano), profile.Onion, testMessage)
	}

	fmt.Println("Shutdown v0 profile store...")
	ps1.Shutdown()

	fmt.Println("New v1 Profile store...")
	ps2, err := LoadProfileWriterStore(eventBus, testingDir, password)
	if err != nil {
		t.Errorf("Error createing new profileStore with new password: %v\n", err)
		return
	}

	profile2 := ps2.GetProfileCopy(true)

	if profile2.Groups[groupid] == nil {
		t.Errorf("Failed to load group %v\n", groupid)
		return
	}

	if len(profile2.Groups[groupid].Timeline.Messages) != 200 {
		t.Errorf("Failed to load group's 200 messages, instead got %v\n", len(profile2.Groups[groupid].Timeline.Messages))
	}
}

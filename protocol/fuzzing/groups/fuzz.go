package groups

import (
	"crypto/rand"
	"cwtch.im/cwtch/model"
	"golang.org/x/crypto/nacl/secretbox"
	"io"
)

// Fuzz various group related functions
func Fuzz(data []byte) int {
	profile := model.GenerateNewProfile("fuzz")
	inviteid, err := profile.ProcessInvite(string(data), profile.Onion)

	if err != nil {
		if inviteid != "" {
			panic("should not have added a group on err")
		}
		return 1
	}

	id, _, _ := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	var nonce [24]byte
	io.ReadFull(rand.Reader, nonce[:])
	encrypted := secretbox.Seal(nonce[:], data, &nonce, &profile.GetGroup(id).GroupKey)

	ok, _, _, _ := profile.AttemptDecryption(encrypted, data)
	if ok {
		panic("this probably shouldn't happen")
	}
	ok = profile.VerifyGroupMessage(string(data), string(data), string(data), 0, encrypted, data)
	if ok {
		panic("this probably shouldn't happen")
	}
	return 0
}
